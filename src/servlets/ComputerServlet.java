package servlets;

import dao.ComputerDao;
import domain.Computer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = "/index")
public class ComputerServlet extends HttpServlet {
    private List<String> messages;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("command")=="update"){
            req.setAttribute("computer",ComputerDao.getOne(req.getAttribute("ser").toString()));
            req.getRequestDispatcher("ComputerEdit.jsp").forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (checkValidation(req)) {
            try {
                String serialNo = req.getParameter("serialNo");//needs validation
                String model = req.getParameter("model");
                String category = req.getParameter("category");
                String processor = req.getParameter("processor");
                int ram = Integer.parseInt(req.getParameter("ram"));//needs validation
                int hardDisk = Integer.parseInt(req.getParameter("hardDisk"));
                Date releaseDate = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("releaseDate"));
                double price = Double.parseDouble(req.getParameter("price"));
                ComputerDao.save(new Computer(serialNo, model, category, processor, ram, hardDisk, releaseDate, price));
                req.setAttribute("saved","Computer Well recorded");
            } catch (ParseException e) {
                messages.add("Please provide inputs");
            }
        }else
            req.setAttribute("validations",messages);
        req.getRequestDispatcher("ComputerForm.jsp").forward(req, resp);
    }
    private boolean checkValidation(HttpServletRequest req) {
        messages= new ArrayList<>();
        boolean valid=true;
        try {
            String serialNo = req.getParameter("serialNo");
            String ram1 = req.getParameter("ram");
            int ram = Integer.parseInt(ram1);
            boolean result = ram > 0 && ((ram & (ram - 1)) == 0);
            if (!result){
                messages.add("ram must be in format of 2^n");
                valid = false;
            }

            if (ComputerDao.getOne(serialNo) != null) {
                messages.add("Computer already exists");
                valid = false;
            }
        } catch (Exception e) {
            messages.add("Please provide inputs");
        }

        return valid;


    }
}
