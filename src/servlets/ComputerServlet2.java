package servlets;

import dao.ComputerDao;
import domain.Computer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = "/update")
public class ComputerServlet2 extends HttpServlet {
    private List<String> messages;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (checkValidation(req)) {
            try {
                String serialNo = req.getParameter("serialNo");//needs validation
                String model = req.getParameter("model");
                String category = req.getParameter("category");
                String processor = req.getParameter("processor");
                int ram = Integer.parseInt(req.getParameter("ram"));//needs validation
                int hardDisk = Integer.parseInt(req.getParameter("hardDisk"));
                Date releaseDate = new SimpleDateFormat("yyyy-MM-dd").parse(req.getParameter("releaseDate"));
                double price = Double.parseDouble(req.getParameter("price"));
                ComputerDao.update(new Computer(serialNo, model, category, processor, ram, hardDisk, releaseDate, price));
                req.setAttribute("updated", "Computer Well recorded");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else
            req.setAttribute("validations", messages);
        req.getRequestDispatcher("ComputerList.jsp").forward(req, resp);
    }

    private boolean checkValidation(HttpServletRequest req) {
        messages = new ArrayList<>();
        boolean valid = true;
        String serialNo = req.getParameter("serialNo");
        int ram = Integer.parseInt(req.getParameter("ram"));
        boolean result = ram > 0 && ((ram & (ram - 1)) == 0);
        if (!result) {
            messages.add("ram must be in format of 2^n");
            valid = false;
        }


        return valid;


    }
}

