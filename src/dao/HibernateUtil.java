package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtil {
    private static SessionFactory sessionFactory;

    public static Session getSession() {
        setSessionFactory();
        return sessionFactory.openSession();
    }

    private static void setSessionFactory() {
        HibernateUtil.sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public static void shutDown() {
        sessionFactory.close();
    }
}

