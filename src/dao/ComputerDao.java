package dao;

import domain.Computer;
import org.hibernate.Session;

import java.util.List;

public class ComputerDao {
    public static void save(Computer computer) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(computer);
        session.getTransaction().commit();
        HibernateUtil.shutDown();
    }

    public static void update(Computer computer) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(computer);
        session.getTransaction().commit();
        HibernateUtil.shutDown();
    }

    public static List<Computer> getAll() {
        Session session = HibernateUtil.getSession();
        List<Computer> computers = session.createQuery("from Computer").list();
        HibernateUtil.shutDown();
        return computers;
    }

    public static void delete(Computer computer) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(computer);
        session.getTransaction().commit();
        HibernateUtil.shutDown();
    }

    public static Computer getOne(String id) {
        Session session = HibernateUtil.getSession();
        Computer computer = session.get(Computer.class, id);
        HibernateUtil.shutDown();
        return computer;
    }

    public static void deleteById(String id) {
        Computer computer = getOne(id);
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(computer);
        session.getTransaction().commit();
        HibernateUtil.shutDown();
    }
}
