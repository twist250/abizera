package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Computer {
    @Id
    private String serialNo;
    private String model;
    private String category;
    private String processor;
    private int ram;
    private int hardDisk;
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    private double price;

}
