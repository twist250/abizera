<%@ page import="dao.ComputerDao" %>
<%@ page import="domain.Computer" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: twist
  Date: 10/16/2018
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All computers</title>
</head>
<body>
<pre>
    <div style="display: flex;justify-content: center">
        <div>
    <table border="1" style="border-collapse: collapse;">
    <thead>
    <tr>
        <th>Serial Number</th>
        <th>Category</th>
        <th>Model</th>
        <th>Capacity</th>
        <th>Price</th>
        <th>Release Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <%
        List<Computer> computers = ComputerDao.getAll();
        for (Computer computer : computers) {
    %>
    <tr>
        <td><%=computer.getSerialNo()%></td>
        <td><%=computer.getCategory()%></td>
        <td><%=computer.getModel()%></td>
        <td><%=computer.getProcessor()+","+computer.getRam()+","+computer.getRam()%></td>
        <td><%=NumberFormat.getInstance().format(computer.getPrice())+"$"%></td>
        <td><%=new SimpleDateFormat("dd/MM/yyyy").format(computer.getReleaseDate())%></td>
        <td><a href="ComputerEdit.jsp?ser=<%=computer.getSerialNo()%>">update</a></td>
    </tr>
    <%}%>
    </tbody>
</table>
<a href="ComputerForm.jsp">Record a computer</a>
            </div>
    </div>
</pre>
</body>
</html>
