<%@ page import="java.util.List" %>
<%@ page import="domain.Computer" %>
<%@ page import="dao.ComputerDao" %><%--
  Created by IntelliJ IDEA.
  User: twist
  Date: 10/16/2018
  Time: 7:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%

    Computer computer = ComputerDao.getOne(request.getParameter("ser"));
%>
<pre>
    <fieldset style="width: 35%">
      <legend>Computer Edit</legend>
      <form action="update" method="post">
        <table>
          <tr>
            <td>SERIALNO:</td><td><input type="text" name="serialNo" value=<%=computer.getSerialNo()%>></td>
          </tr>
          <tr>
            <td>MODEL:</td><td><input type="text" name="model" value=<%=computer.getModel()%>></td>
          </tr>
          <tr>
            <td>CATEGORY:</td><td><input type="text" name="category" value=<%=computer.getCategory()%>></td>
          </tr>
          <tr>
            <td>PROCESSOR:</td><td><input type="text" name="processor" value=<%=computer.getProcessor()%>></td>
          </tr>
          <tr>
            <td>RAM:</td><td><input type="number" name="ram" value=<%=computer.getRam()%>></td>
          </tr>
          <tr>
            <td>HARDDISK:</td><td><input type="number" name="hardDisk" value=<%=computer.getHardDisk()%>></td>
          </tr>
          <tr>
            <td>RELEASE DATE:</td><td><input type="date" name="releaseDate" value=<%=computer.getReleaseDate()%>></td>
          </tr>
          <tr>
            <td>price:</td><td><input type="number" name="price" value=<%=computer.getPrice()%>></td>
          </tr>
        </table>
        <button type="submit">UPDATE</button>
      </form>
      <ul>
        <%
            if (request.getAttribute("validations") != null) {
                List<String> validations = (List<String>) request.getAttribute("validations");
                for (String validation : validations) {
        %>
        <li style="color: tomato"><%=validation%></li>
        <%
                }
                request.removeAttribute("validations");
            }
        %>
        <%
            if (request.getAttribute("updated")!=null){
        %>
        <li style="color:green;"><%=request.getAttribute("saved")%></li>
          <%}%>
      </ul>
    </fieldset>
    <a href="ComputerList.jsp">View All...</a>
  </pre>
</body>
</html>
