<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: twist
  Date: 10/16/2018
  Time: 6:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
<pre>
    <fieldset style="width: 35%">
      <legend>Computer saving</legend>
      <form action="index" method="post">
        <table>
          <tr>
            <td>SERIALNO:</td><td><input type="text" name="serialNo"></td>
          </tr>
          <tr>
            <td>MODEL:</td><td><input type="text" name="model"></td>
          </tr>
          <tr>
            <td>CATEGORY:</td><td><input type="text" name="category"></td>
          </tr>
          <tr>
            <td>PROCESSOR:</td><td><input type="text" name="processor"></td>
          </tr>
          <tr>
            <td>RAM:</td><td><input type="number" name="ram"></td>
          </tr>
          <tr>
            <td>HARDDISK:</td><td><input type="number" name="hardDisk"></td>
          </tr>
          <tr>
            <td>RELEASE DATE:</td><td><input type="date" name="releaseDate"></td>
          </tr>
          <tr>
            <td>price:</td><td><input type="number" name="price"></td>
          </tr>
        </table>
        <button type="submit">SAVE</button>
      </form>
      <ul>
        <%
            if (request.getAttribute("validations") != null) {
                List<String> validations = (List<String>) request.getAttribute("validations");
                for (String validation : validations) {
        %>
        <li style="color: tomato"><%=validation%></li>
        <%
                }
                request.removeAttribute("validations");
            }
        %>
        <%
          if (request.getAttribute("saved")!=null){
        %>
        <li style="color:green;"><%=request.getAttribute("saved")%></li>
        <%}%>
      </ul>
    </fieldset>
    <a href="ComputerList.jsp">View All...</a>
  </pre>
</body>
</html>
